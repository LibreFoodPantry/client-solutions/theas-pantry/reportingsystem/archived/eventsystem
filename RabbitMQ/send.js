#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

var data = {
    name: 'Austin Engel',
    company: 'JP Morgan',
    designation: 'Senior Application Engineer'
}

var data2 = {
    name: 'Derin Sabu',
    company: 'Google',
    designation: 'Data Scientist'
};


amqp.connect('amqp://rabbitmq', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }

        var queue = 'ReportingTeam';

        channel.assertQueue(queue, {
            durable: false
        });
        channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
        channel.sendToQueue(queue, Buffer.from(JSON.stringify(data2)));
        console.log(`message sent to ${queue}`);
    });
    setTimeout(function() {
        connection.close();
        process.exit(0);
    }, 500);
});